/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplobd;


import java.sql.*;

import javax.swing.JOptionPane;
/**
 *
 * @author ize
 */
public class EjemploBD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //liga dle driver https://www.mysql.com/products/connector/
        //Declaracion de 
        String db = "jdbc:mysql://127.0.0.1:3306/conJava";
        String user = "root";
        String pass = "notsecret";

        try {
            Connection conexion = DriverManager.getConnection(db,user,pass);
            
            Statement instruccion = conexion.createStatement();
            
            String sql ="SELECT * from persona WHERE id_persona = 1";
            
            ResultSet resultado = instruccion.executeQuery(sql);
            
            while(resultado.next()){

                JOptionPane.showMessageDialog(null,"nombre: " + 
                        resultado.getString("nombre") );
            }
   
            
        } catch (SQLException e) {
            System.out.println("Error: " + e);
        }

    }

}
